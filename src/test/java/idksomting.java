import java.util.concurrent.TimeUnit;

import components.Footer;
import components.QuickViewOverlay;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.List;
import org.openqa.selenium.interactions.Actions;
import pageojects.HomePage;
import  components.Header;
import pageojects.ProductListingPage;


/**
 * Created by geraldcarlton89 on 5/24/16.
 */



public class idksomting {

    WebDriver driver;
    List<WebElement> FooterLinks;
    WebElement searchBox;
    WebElement SearchButton;
    WebElement firstProduct;
    WebElement quickView1;
    WebElement qtyDropdown;
    WebElement addToCartBtn;
    WebElement continueShopBtn;
    WebElement cartBtn;
    WebElement editBtn;


    public boolean isJqueryRunning(WebDriver driver) {

        final String code = "return jQuery.active === 1;";
        try {
            return Boolean.valueOf(String.valueOf(((JavascriptExecutor) driver).executeScript(code)));
        } catch (WebDriverException e) {
            if (e.getMessage().contains("jQuery is not defined")) {
                return false;
            } else {
                throw e;

                }
        }

    }

    public void waitForjQueryAjax(WebDriver driver) {

        int timeWaited = 0;
        while (isJqueryRunning(driver)
                && (timeWaited < 20)) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e)

            {
                System.err.println(e);
            }
            ++timeWaited;

        }
    }


    public void openBrowser(){

        driver = new FirefoxDriver();
        driver.get("http://ca.motivescosmetics.com/");
        driver.manage().window().maximize();
    }

    public void setFooterLinks(){

        FooterLinks = driver.findElements(By.cssSelector(".social nav a"));
    }

    public void clickFooterLink(int index){
        setFooterLinks();
        FooterLinks.get(index).click();
        waitForjQueryAjax(driver);

    }

    public WebElement findElement(String elementDefinition)
    {
        return driver.findElement(By.cssSelector(elementDefinition));
    }

    public List<WebElement> findElements(String elementDefinition)
    {
        return driver.findElements(By.cssSelector(elementDefinition));
    }

    public Boolean does_element_exist(String elementDefinition)
    {
        return findElements(elementDefinition).size()>0;
    }


    @Test
    public void footerLinkTest() {

        Footer footer = new Footer(driver);

        footer.clickFooterLink(6);
        Assert.assertTrue(does_element_exist("#termsOfUse"));


        footer.clickFooterLink(7);
        Assert.assertTrue(does_element_exist("#privacyLayout"));


        footer.clickFooterLink(8);
        Assert.assertTrue(does_element_exist("[itemprop='description']"));


        footer.clickFooterLink(9);
        Assert.assertTrue(does_element_exist("#shippingPolicy"));


        footer.clickFooterLink(10);
        Assert.assertTrue(does_element_exist("#returnLayout"));

        driver.quit();


    }



    @Test
    public void QuickViewTest(){

        HomePage home = new HomePage(driver);
        home.openBrowser();

        Header header = new Header(driver);
        header.searchForTerm();

        ProductListingPage productListingPage = new ProductListingPage(driver);
        productListingPage.getTheProduct();

        waitForjQueryAjax(driver);

        QuickViewOverlay quickViewOverlay = new QuickViewOverlay(driver);
        quickViewOverlay.setQtyQuickView();

        driver.quit();

    }



}


