package pageojects;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;


/**
 * Created by geraldcarlton89 on 6/15/16.
 */
public class HomePage extends BasePage {



    public HomePage(WebDriver driver) {

        super(driver);

    }


    public void openBrowser(){

        driver = new FirefoxDriver();
        driver.get("http://ca.motivescosmetics.com/");
        driver.manage().window().maximize();
    }



}
