package pageojects;

import components.Footer;
import components.Header;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

/**
 * Created by geraldcarlton89 on 6/24/16.
 */
public class BasePage{

    WebDriver driver;
    Header header;
    Footer footer;


    public  BasePage(WebDriver driver){

        this.driver = driver;
        header = new Header(driver);
        footer = new Footer(driver);
    }



    public WebElement findElement(String elementDefinition)
    {
        return driver.findElement(By.cssSelector(elementDefinition));
    }

    public List<WebElement> findElements(String elementDefinition)
    {
        return driver.findElements(By.cssSelector(elementDefinition));
    }



    public boolean isJqueryRunning(WebDriver driver) {

        final String code = "return jQuery.active === 1;";
        try {
            return Boolean.valueOf(String.valueOf(((JavascriptExecutor) driver).executeScript(code)));
        } catch (WebDriverException e) {
            if (e.getMessage().contains("jQuery is not defined")) {
                return false;
            } else {
                throw e;

            }
        }

    }

    public void waitForjQueryAjax(WebDriver driver) {

        int timeWaited = 0;
        while (isJqueryRunning(driver)
                && (timeWaited < 20)) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e)

            {
                System.err.println(e);
            }
            ++timeWaited;

        }
    }

    public void mouseHover(WebElement element){

        Actions hover = new Actions(driver);

        hover.moveToElement(element).build().perform();

    }

    public void retryClick(WebElement element) {

        try {
            waitForjQueryAjax(driver);
            element.click();

        } catch (WebDriverException e) {
            System.out.print("Retrying click");

            element.click();
        }
    }

    public Boolean does_element_exist(String elementDefinition)
    {
        return findElements(elementDefinition).size()>0;
    }



    public Header getHeader(){

        return header;

}


    public  Footer  getFooter(){

        return footer;
    }


}
