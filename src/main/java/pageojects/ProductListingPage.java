package pageojects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by geraldcarlton89 on 6/15/16.
 */
public class ProductListingPage extends BasePage {

    WebElement firstProduct;
    WebElement quickView1;
    String firstProductCss = "[itemprop='image']";
    String quickViewCss = ".btn.btn-primary.preview";


    public ProductListingPage(WebDriver driver){

        super(driver);

    }



public void getTheProduct() {


    firstProduct = findElements(firstProductCss).get(0);

    mouseHover(firstProduct);
    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

    quickView1 = findElements(quickViewCss).get(0);
    quickView1.click();


    }

}
