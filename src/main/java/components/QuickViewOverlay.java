package components;

import org.openqa.selenium.*;
import org.testng.Assert;
import pageojects.BasePage;

/**
 * Created by Tiffany on 6/24/2016.
 */
public class QuickViewOverlay extends BasePage {


    WebElement qtyDropdown;
    WebElement addToCartBtn;
    WebElement continueShopBtn;
    WebElement cartBtn;
    WebElement editBtn;
    WebDriver driver;

    String qtyDropdownCss = "[id='qty']";
    String cartBtnCss = ".cart";
    String editBtnCss = ".edit";
    String addToCartBtnCss = ".btn.btn-primary.cart";
    String continueShopBtnCss = ".offset-1 [data-dismiss='modal']";
    String qtyFieldCss = "[name='prdQty1']";

    public QuickViewOverlay (WebDriver driver){

        super(driver);

    }
    

    public void setQuanity(WebElement element, String qty){

        element.click();

        element.sendKeys(qty);

        element.click();
    }


    public void setQtyQuickView(){

        qtyDropdown = findElement(qtyDropdownCss);

        setQuanity(qtyDropdown,"2");


        addToCartBtn = findElement(addToCartBtnCss);
        addToCartBtn.click();

        continueShopBtn = findElement(continueShopBtnCss);
        continueShopBtn.click();

        cartBtn = findElement(cartBtnCss);
        retryClick(cartBtn);

        editBtn = findElement(editBtnCss);
        waitForjQueryAjax(driver);
        editBtn.click();

        String quantityField = findElement(qtyFieldCss).getAttribute("value");
        Assert.assertEquals("2", quantityField);


    }

}
