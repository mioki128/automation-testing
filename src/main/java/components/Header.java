package components;

import org.openqa.selenium.*;
import pageojects.BasePage;

/**
 * Created by Tiffany on 6/24/2016.
 */
public class Header extends BasePage{

    WebDriver driver;
    WebElement searchBox;
    WebElement SearchButton;
    WebElement cartBtn;
    WebElement editBtn;


    String cartBtnCss = ".cart";
    String editBtnCss = ".edit";
    String SearchButtonCss = ".btn.search";
    String searchBoxCss = ".search-box [name='keyword']";


    public Header(WebDriver driver){

        super(driver);


    }



    public void searchForTerm(){

        searchBox = findElement(searchBoxCss);
        searchBox.click();
        searchBox.sendKeys("eye");
        SearchButton = findElement(SearchButtonCss);
        SearchButton.click();

    }



    public void clickEditCartBtn(){

        cartBtn = findElement(cartBtnCss);
        retryClick(cartBtn);

        editBtn = findElement(editBtnCss);
        waitForjQueryAjax(driver);
        editBtn.click();
    }

}
