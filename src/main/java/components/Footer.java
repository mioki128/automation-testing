package components;

import org.openqa.selenium.*;
import pageojects.BasePage;

import java.util.List;

/**
 * Created by Tiffany on 6/24/2016.
 */
public class Footer  extends BasePage{

    List<WebElement> FooterLinks;
    String footerLinksCss = ".social nav a";
    WebDriver driver;

    public Footer(WebDriver driver){

        super(driver);

    }



    public void setFooterLinks(){

        FooterLinks = driver.findElements(By.cssSelector(footerLinksCss));
    }

    public void clickFooterLink(int index){
        setFooterLinks();
        FooterLinks.get(index).click();
        waitForjQueryAjax(driver);

    }


}
