
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;

/**
 * Created by Tiffany on 5/24/2016.
 */
public class TestingTestingOneTwoThree
{
    Actions action;
    WebDriver web_functions;
    WebElement search_box;
    WebElement search_button;
    WebElement filter_link;
    WebElement filter_clear;
    WebElement quick_element_preview;
    WebElement product_image;
    WebElement quantity_box;
    WebElement add_to_cart;
    WebElement cont;
    WebElement cart_btn;
    WebElement edit;

    String quantity = "2";
    String item_id = "tls";


    @BeforeMethod(alwaysRun = true)
    public void open_browser()
    {
        //navigate to home method name
        web_functions = new FirefoxDriver();
        web_functions.get("http://www.tlsslim.com");
    }

    public void click_search_box()
    {

        search_box = findElement("#header-search"); //actual search box
        search_box.click();
    }


    //
    public void search_item()
    {
        search_box.sendKeys(item_id);
        search_button = findElement(".icon-search"); //search button
        search_button.click();
    }


    public void apply_filter() //apply_filter
    {
        //a list of filters and this will choose second item
        filter_link = findElements("ul .reset > a").get(1); 
        filter_link.click();
        
    }
    
    public void clear_filters()
    {
        
        filter_clear = findElement(".filter-container > a");
        filter_clear.click();
    }

    /**
     *Sets web element for the quick view to first product in a list of products */ //put above my methods
    public void quick_view_image() //sets web element definition for quick view
    {
        //gets the first product from a list of products
        product_image = findElements(".product-image").get(0);
    }
    

    /**
     *Action moves the mouse to the element specified in the parameter*/
    public void move_mouse_to_element( WebElement quickView_image)
    {

        //use actions move the mouse
        action = new Actions(web_functions);
        action.moveToElement(quickView_image);
        action.build().perform();
    }

    public void click_quick_view() //click_quickView
    {
        quick_element_preview =  findElement(".btn-quick"); //element for quickview button
        quick_element_preview.click();
    }

    public void enter_quantity() //enter_quantity //accept quantity as a parameter
    {
        wait_a_sec();
        quantity_box = findElement("#qty"); //element for quantity box
        quantity_box.click();
        quantity_box.sendKeys(quantity); //send the quantity
        quantity_box.click();
    }
    /**

     *This method will add the item to the cart from the modal*/
    public void qv_add_to_cart()
    {
        wait_a_sec();

        add_to_cart = findElement(".btn-primary.cart");
        add_to_cart.click();
    }

    /**

     *This method will click on the 'X' icon to continue shopping*/
    public void continue_shopping() //click_continue_shopping
    {
        wait_a_sec();
        cont = findElement(".modal-content>span");
        cont.click();
    }

    public void go_to_cart()
    {
        wait_a_sec();
        cart_btn = findElement(".icon-cart");
        cart_btn.click();
    }

    public void edit_cart()
    {
        wait_a_sec();
        edit = findElement(".edit");
        edit.click();
    }


    public void wait_a_sec()
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public WebElement findElement(String elementDefinition)
    {
        return web_functions.findElement(By.cssSelector(elementDefinition));
    }

    public List<WebElement> findElements(String elementDefinition)
    {
        return web_functions.findElements(By.cssSelector(elementDefinition));
    }

    public Boolean existing_element(String elementDefinition)
    {
        return findElements(elementDefinition).size()>0;
    }


    @AfterMethod(alwaysRun = true)
    public void close_browser()
    {
        web_functions.quit();
    }
    /*

    TEST 1 - 383


     */
    @Test
    public void test_search_functionality()
    {
        //Verify we are on the homepage
        Assert.assertTrue(existing_element(".carousel-inner"));

        click_search_box();

        search_item();

        //Verify we searched for the item
        Assert.assertTrue(existing_element(".result"));

        apply_filter();
        clear_filters();

    }

    /*

    TEST 2 - ASQA-550

     */

    @Test
    public void quick_view()
    {

        click_search_box();

        search_item();

        quick_view_image();

        move_mouse_to_element(product_image);

        click_quick_view();

        enter_quantity();

        //Verify Product Name displays
        Assert.assertTrue(existing_element(".details > a"));


        //Verify Product Price displays
        Assert.assertTrue(existing_element(".cart.price"));

        qv_add_to_cart();

        wait_a_sec();

        //Verify the mini cart displays
        Assert.assertTrue(existing_element(".alert-success"));

        continue_shopping();

        go_to_cart();

        edit_cart();

        //Verify quantity of item in cart is 2
        wait_a_sec();
        Assert.assertTrue(existing_element("[data-qty='2']"));

    }

}